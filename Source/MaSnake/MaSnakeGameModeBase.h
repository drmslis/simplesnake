// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MaSnakeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MASNAKE_API AMaSnakeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
